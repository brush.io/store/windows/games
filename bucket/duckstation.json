{
    "version": "2024.05.05-g6b03943",
    "description": "A fast PlayStation 1 emulator for PC and Android",
    "homepage": "https://github.com/stenzek/duckstation/",
    "license": {
        "identifier": "GPL-3.0-only",
        "url": "https://github.com/stenzek/duckstation/blob/master/LICENSE"
    },
    "notes": [
        "! Duckstation requires a PSX BIOS to function.",
        "• Place the BIOS file in \"$persist_dir/data/bios\".",
        "• Learn more at: https://www.duckstation.org/wiki/BIOS"
    ],
    "url": "https://github.com/stenzek/duckstation/releases/download/latest/duckstation-windows-x64-release.zip",
    "hash": "c50fae6a8f09583728d2c6c18e38cfbb3d7a03943034598ad890450d37765eb6",
    "pre_install": [
        "$ErrorActionPreference = \"SilentlyContinue\"",
        "New-item \"$env:XDG_CACHE_HOME\" -ItemType Directory -Name  \"$app\"                     -Force | Out-Null",
        "New-item \"$env:XDG_DATA_HOME\"  -ItemType Directory -Name  \"$app\"                     -Force | Out-Null",
        "New-item \"$persist_dir/cache\"  -ItemType Junction  -Value \"$env:XDG_CACHE_HOME/$app\" -Force | Out-Null",
        "New-item \"$persist_dir/data\"   -ItemType Junction  -Value \"$env:XDG_DATA_HOME/$app\"  -Force | Out-Null"
    ],
    "installer": {
        "script": [
            "$ErrorActionPreference = \"SilentlyContinue\"",
            "Remove-Item \"$dir/updater.exe\""
        ]
    },
    "bin": [
        [
            "duckstation-qt-x64-ReleaseLTCG.exe",
            "duckstation"
        ]
    ],
    "shortcuts": [
        [
            "../2024.05.05-g6b03943/duckstation-qt-x64-ReleaseLTCG.exe",
            "Duckstation"
        ]
    ],
    "persist": [
        [
            "cache",
            "cache/cache"
        ],
        [
            "dump",
            "cache/dump"
        ],
        [
            "bios",
            "data/bios"
        ],
        [
            "cheats",
            "data/cheats"
        ],
        [
            "covers",
            "data/covers"
        ],
        [
            "gamesettings",
            "data/gamesettings"
        ],
        [
            "inputprofiles",
            "data/inputprofiles"
        ],
        [
            "memcards",
            "data/memcards"
        ],
        [
            "savestates",
            "data/savestates"
        ],
        [
            "screenshots",
            "data/screenshots"
        ],
        [
            "shaders",
            "data/shaders"
        ],
        [
            "textures",
            "data/textures"
        ],
        [
            "settings.ini",
            "data/settings.ini"
        ]
    ],
    "pre_uninstall": [
        "$ErrorActionPreference = \"SilentlyContinue\"",
        "Copy-Item \"$dir/settings.ini\" -Destination \"$persist_dir/data/settings.ini\" -Force"
    ],
    "post_uninstall": [
        "$ErrorActionPreference = \"SilentlyContinue\"",
        "Remove-Item \"$persist_dir\" -Recurse -Force"
    ],
    "checkver": {
        "url": "https://github.com/stenzek/duckstation/releases/tag/latest",
        "regex": " datetime=\"(?<year>[0-9]{4})-(?<month>[0-9]{2})-(?<day>[0-9]{2})[\\s\\S]*?.*duckstation/commit/(?<commit>[0-9a-f]{7})",
        "replace": "${year}.${month}.${day}-g${commit}"
    },
    "autoupdate": {
        "url": "https://github.com/stenzek/duckstation/releases/download/latest/duckstation-windows-x64-release.zip",
        "shortcuts": [
            [
                "../$version/duckstation-qt-x64-ReleaseLTCG.exe",
                "Duckstation"
            ]
        ]
    }
}
